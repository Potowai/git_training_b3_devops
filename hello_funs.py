def say_something_to_someone(something, someone):
	print(something + " " + someone)

def say_hello_world():
	say_something_to_someone("hello world", "")

def say_happy_new_year(name):
	say_something_to_someone("happy new year", name)

def say_happy_birthday(name):
	say_something_to_someone("HB", name)
